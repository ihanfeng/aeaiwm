package com.agileai.wm.module.system.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.wm.cxmodule.SecurityGroupTreeSelect;

public class SecurityGroupTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityGroupTreeSelect {
    public SecurityGroupTreeSelectImpl() {
        super();
    }
}
