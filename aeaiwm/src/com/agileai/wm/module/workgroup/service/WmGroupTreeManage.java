package com.agileai.wm.module.workgroup.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.QueryModelService;
import com.agileai.hotweb.bizmoduler.core.TreeManage;

public interface WmGroupTreeManage
        extends TreeManage,QueryModelService {
	
	public void addRelation(String grpId,List<String> userIdList,String empJob);
	public void removeRelation(String grpId,String userId);
	public void updateRelProperties(DataParam param);
	public int getMaxEmpSort(String grpId);
}
