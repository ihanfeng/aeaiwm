package com.agileai.wm.module.workgroup.handler;

import java.util.Date;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManage;
import com.agileai.hotweb.controller.core.TreeManageHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;
import com.agileai.wm.module.workgroup.service.WmGroupTreeManage;

public class WmGroupTreeManageHandler
        extends TreeManageHandler {
    public WmGroupTreeManageHandler() {
        super();
        this.serviceId = buildServiceId(WmGroupTreeManage.class);
        this.nodeIdField = "GRP_ID";
        this.nodePIdField = "GRP_PID";
        this.moveUpErrorMsg = "该节点是第一个节点，不能上移！";
        this.moveDownErrorMsg = "该节点是最后一个节点，不能下移！";
        this.deleteErrorMsg = "该节点还有子节点，不能删除！";
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        WmGroupTreeManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords, "GRP_ID",
                                                  "GRP_NAME", "GRP_PID");

        return treeBuilder;
    }
    
	public ViewRenderer prepareDisplay(DataParam param) {
		String nodeId = param.get(this.nodeIdField);
		if (StringUtil.isNullOrEmpty(nodeId)){
			nodeId = provideDefaultNodeId(param);
		}
		String grpState = param.get("grpState");
		if("null".equals(grpState) || null == grpState){
			param.put("grpState", "1");
		}
		
		TreeBuilder treeBuilder = provideTreeBuilder(param);
		TreeModel treeModel = treeBuilder.buildTreeModel();
		
		TreeManage service = this.getService();
		DataParam queryParam = new DataParam(this.nodeIdField,nodeId);
		DataRow record = service.queryCurrentRecord(queryParam);
		this.setAttributes(record);	
		this.processPageAttributes(param);
		this.setAttribute(this.nodeIdField, nodeId);
		this.setAttribute("isRootNode",String.valueOf(isRootNode(param)));
		
		String menuTreeSyntax = this.getTreeSyntax(param,treeModel,new StringBuffer());
		this.setAttribute("menuTreeSyntax", menuTreeSyntax);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
        setAttribute("GRP_TYPE",
                     FormSelectFactory.create("GROUP_TYPE")
                                      .addSelectedValue(getAttributeValue("GRP_TYPE")));
        setAttribute("GRP_STATE",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getAttributeValue("GRP_STATE")));
        setAttribute("CHILD_GRP_TYPE",
                     FormSelectFactory.create("GROUP_TYPE")
                                      .addSelectedValue(getAttributeValue("CHILD_GRP_TYPE",
                                                                          "1")));
        setAttribute("CHILD_GRP_STATE",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getAttributeValue("CHILD_GRP_STATE",
                                                                          "1")));
        
        setAttribute("CHILD_GRP_START_TIME",DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date()));     
        
        setAttribute("currentTabId",param.get("currentTabId"));
        setAttribute("currentTabId",getAttribute("currentTabId", "0"));
        
        setAttribute("grpState",
                FormSelectFactory.create("SYS_VALID_TYPE")
                                 .addSelectedValue(param.get("grpState")));
    }

    protected String provideDefaultNodeId(DataParam param) {
        return "00000000-0000-0000-00000000000000000";
    }

    protected boolean isRootNode(DataParam param) {
        boolean result = true;
        String nodeId = param.get(this.nodeIdField,
                                  this.provideDefaultNodeId(param));
        DataParam queryParam = new DataParam(this.nodeIdField, nodeId);
        DataRow row = this.getService().queryCurrentRecord(queryParam);

        if (row == null) {
            result = false;
        } else {
            String parentId = row.stringValue("GRP_PID");
            result = StringUtil.isNullOrEmpty(parentId);
        }

        return result;
    }

    protected WmGroupTreeManage getService() {
        return (WmGroupTreeManage) this.lookupService(this.getServiceId());
    }
}
