package com.agileai.wm.module.workmanage.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;

public interface WmWeekManage
        extends MasterSubService {

	List<DataRow> getGroupRecords(String userId);
	
	List<DataRow> findNofinishWeeks(String weekId,String userId);
	List<DataRow> findLastWeekPresRecord(String weekId,String userId);
	List<DataRow> findWeekWorkPres(String preId);
	List<DataRow> checkWeekTime(DataParam param);
	List<DataRow> insertWeekWorkEntry(List<DataRow> newEntry);
	List<DataRow> findEntry(String wwId);
	DataRow getBeforeWeekRow(String weekId);
	DataRow findMaxEntrySort(String wwId);
	DataRow findMaxPreSort(String wwId);
	public void updateStateRecord(DataParam param);
	public void updateEntryState(DataParam param);
	String findPower(String userId);
	List<DataRow> getPersonalWmWeek(DataParam param);
	List<DataRow> getWeekWork(DataParam param);
	
	List<DataRow> insertWeekWorkEntryParam(List<DataRow> newEntryList);
	List<DataRow> insertWeekWorkPrepareParam(List<DataRow> newEntryList);
	List<DataRow> findWeekWorkEntry(String entryId);
	List<DataRow> insertWeekWorkPrepare(List<DataRow> newEntry);
//	List<DataRow> insertWeekWorkPrePareRecords(List<DataRow> newPrepare);
	DataRow findGroupRecord(DataParam param);
	List<DataRow> getEmpJobRecords(String userId,String groupId);
	
	void insertWWPrepareParam(List<DataParam> params);
	void insertWWEntryParam(List<DataParam> params);
	
	DataRow getCurrentWeek(String currentDate);
	DataRow getBeforeWeekRowInfo(String weekId);
	public DataRow getNextWeekRow(String weekId);
	DataRow getCurrentWeekRow(String weekTimeId);
	List<DataRow> getWeekWorkRecord(String userId, String weekId);
	List<DataRow> getWeekPrepareRecord(String userId, String weekId);
	DataRow getMasterWeekWorkRecord(String userId, String weekId);
	List<DataRow> findUserRecords();
	List<DataRow> findWorkWeekExaminationListInfos(String userId);
	public DataRow getUserRecord(String userId);
	List<DataRow> findweekCompRecords(String sdate,String edate,String userId);
	DataRow getEntryWorkNumRecord(String userId, String weekId);
	List<DataRow> findWeekExaminationUserInfos(String grpId);
	List<DataRow> findWeekExamMasterInfos(String userId);
	List<DataRow> findWeekExamEntryInfos(String weekWorkId);
	List<DataRow> findWeekExamPreInfos(String weekWorkId);
	List<DataRow> initGroupRecords(String userId);
	DataRow findActiveUserId(String userCode);
	
	DataRow getCurrentWeekTimeId(String currentDate);
	List<DataRow> findCurrentGroupUserCodes(String groupId);
	List<DataRow> findStaticUserRecords(String userRoleCode);
	public void doSubmitWeekWork(String weekWorkId,String state);
	public void doSubmitEntryWeekWork(String weekWorkId,String state);
	DataRow getCurrentSelectWeekInfo(String weekWorkId);
	DataRow queryGroupRecord(String grpId);
	List<DataRow> quertUserEmpJobRecords(String userId,String grpId);
}
