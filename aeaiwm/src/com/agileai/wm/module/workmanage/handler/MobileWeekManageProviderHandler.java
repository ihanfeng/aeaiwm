package com.agileai.wm.module.workmanage.handler;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.wm.module.workmanage.service.WmWeekManage;

public class MobileWeekManageProviderHandler extends SimpleHandler{
	private static String ERROR = "error";
	private static String RoleCode = "Leader";
	private static String State = "CONFIRMED";
	private static String ENTRYSTATE = "1";
	
	public MobileWeekManageProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param){
		String responseText = FAIL;
		return new AjaxRenderer(responseText);
	}
	@PageAction
	public ViewRenderer findWeekManageCardInfo(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row1 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			if(row1 != null && row1.size() > 0){
				currentWeekId = row1.getString("WT_ID");
			}else{
				row1 = wmWeekManage.getCurrentWeekTimeId(currentDate);
				currentWeekId = row1.getString("WT_ID");
			}
			
			JSONObject jsonObject = new JSONObject();
			List<DataRow> currentWeekRecords = wmWeekManage.getWeekWorkRecord(userId,currentWeekId);
			JSONArray jsonArray2 = new JSONArray();
			if(currentWeekRecords.size() != 0){
				int size = 5;
				if(currentWeekRecords.size() < 5){
					size = currentWeekRecords.size(); 
				}
				for(int i=0;i<size;i++){
					DataRow row = currentWeekRecords.get(i);
					JSONObject jsonObject2 = new JSONObject();
					jsonObject2.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject2.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonObject2.put("num", i+1);
					jsonArray2.put(jsonObject2);
				}
			}else{
				JSONObject jsonObject2 = new JSONObject();
				jsonObject2.put("num", 1);
				jsonObject2.put("describe", "无记录");
				jsonObject2.put("planday", "0");
				jsonArray2.put(jsonObject2);
			}
			
			jsonObject.put("currentWeek", jsonArray2);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findLastWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			String weekTimeId = param.get("weekTimeId");
			String userId = param.get("userId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			String beforeWeekId = "";
			String startTime = "";
			String endTime = "";
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			
			String grpName = "";
			if(null != grpId){
				DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			DataRow row1 = wmWeekManage.getBeforeWeekRow(weekTimeId);
			if(row1 != null && row1.size()>0){
				beforeWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				
				DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,beforeWeekId);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", startTime);
				jsonObject.put("endTime", endTime);
				jsonObject.put("time", startTime);
				
				jsonObject.put("grpName", grpName);
				jsonObject.put("weekTimeId", beforeWeekId);
				if(weekManageRow != null){
					jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
					jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
					jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
					jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
					String completion =  weekManageRow.stringValue("WW_COMPLETION");
					if("0".equals(completion)){
						jsonObject.put("completion","");
					}else{
						jsonObject.put("completion",completion);
					}
				}else{
					jsonObject.put("wwDay", "0");
					jsonObject.put("weekWorkId","无记录");
					jsonObject.put("stateName", "空");
				}
				List<DataRow> beforeWeekRecords = wmWeekManage.getWeekWorkRecord(userId,beforeWeekId);
				JSONArray jsonArray11 = new JSONArray();
				if(beforeWeekRecords.size() != 0){
					for(int i=0;i<beforeWeekRecords.size();i++){
						DataRow row = beforeWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
						jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
						jsonArray11.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", "无记录");
					jsonObject11.put("planday", "0");
					jsonArray11.put(jsonObject11);
				}
				
				List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,beforeWeekId);
				JSONArray jsonArray12 = new JSONArray();
				if(prepareWeekRecords.size() != 0){
					for(int i=0;i<prepareWeekRecords.size();i++){
						DataRow row = prepareWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
						jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
						jsonArray12.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", "无记录");
					jsonObject11.put("preplanday","0");
					jsonArray12.put(jsonObject11);
				}
				jsonObject.put("plan", jsonArray11);
				jsonObject.put("follow", jsonArray12);
				responseText = jsonObject.toString();
			}else{
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", ERROR);
				jsonObject.put("weekTimeId", weekTimeId);
				responseText = jsonObject.toString();
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findThisWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			String userId = param.get("userId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String grpName = "";
			if(null != grpId){
				DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row1 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			String startTime = "";
			String endTime = "";
			if(row1 != null && row1.size() > 0){
				currentWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}else{
				row1 = wmWeekManage.getCurrentWeekTimeId(currentDate);
				currentWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}
			DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("startTime", startTime);
			jsonObject.put("time", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("weekTimeId", currentWeekId);
			
			jsonObject.put("grpName", grpName);
			if(weekManageRow != null){
				jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
				jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
				jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
				jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
				String completion =  weekManageRow.stringValue("WW_COMPLETION");
				if("0".equals(completion)){
					jsonObject.put("completion","");
				}else{
					jsonObject.put("completion",completion);
				}
			}else{
				jsonObject.put("wwDay", "0");
				jsonObject.put("weekWorkId","无记录");
				jsonObject.put("stateName", "空");
			}
			
			List<DataRow> weekWorkRecords = wmWeekManage.getWeekWorkRecord(userId,currentWeekId);
			JSONArray jsonArray11 = new JSONArray();
			if(weekWorkRecords.size() != 0){
				for(int i=0;i<weekWorkRecords.size();i++){
					DataRow row = weekWorkRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("describe", "无记录");
				jsonObject11.put("planday", "0");
				jsonArray11.put(jsonObject11);
			}
			
			List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,currentWeekId);
			JSONArray jsonArray12 = new JSONArray();
			if(prepareWeekRecords.size() != 0){
				for(int i=0;i<prepareWeekRecords.size();i++){
					DataRow row = prepareWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
					jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
					jsonArray12.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("predesc", "无记录");
				jsonObject11.put("preplanday", "0");
				jsonArray12.put(jsonObject11);
			}
			jsonObject.put("plan", jsonArray11);
			jsonObject.put("follow", jsonArray12);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findFllowWeekWorkListInfo(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			String weekTimeId = param.get("weekTimeId");
			String userId = param.get("userId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			
			String grpName = "";
			if(null != grpId){
				DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			DataRow row1 = wmWeekManage.getNextWeekRow(weekTimeId);
			String followWeekId = "";
			String startTime = "";
			String endTime = "";
			if(row1 != null && row1.size() > 0){
				followWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				
				DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,followWeekId);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", startTime);
				jsonObject.put("time", startTime);
				jsonObject.put("endTime", endTime);
				jsonObject.put("weekTimeId", followWeekId);
				
				jsonObject.put("grpName", grpName);
				if(weekManageRow != null){
					jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
					jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
					jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
					jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
					String completion =  weekManageRow.stringValue("WW_COMPLETION");
					if("0".equals(completion)){
						jsonObject.put("completion","");
					}else{
						jsonObject.put("completion",completion);
					}
				}else{
					jsonObject.put("wwDay", "0");
					jsonObject.put("weekWorkId","无记录");
					jsonObject.put("stateName", "空");
				}
				
				List<DataRow> followWeekRecords = wmWeekManage.getWeekWorkRecord(userId,followWeekId);
				JSONArray jsonArray11 = new JSONArray();
				if(followWeekRecords.size() != 0){
					for(int i=0;i<followWeekRecords.size();i++){
						DataRow row = followWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
						jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
						jsonArray11.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", "无记录");
					jsonObject11.put("planday", "0");
					jsonArray11.put(jsonObject11);
				}
				
				List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,followWeekId);
				JSONArray jsonArray12 = new JSONArray();
				if(prepareWeekRecords.size() != 0){
					for(int i=0;i<prepareWeekRecords.size();i++){
						DataRow row = prepareWeekRecords.get(i);
						JSONObject jsonObject11 = new JSONObject();
						jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
						jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
						jsonArray12.put(jsonObject11);
					}
				}else{
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", "无记录");
					jsonObject11.put("preplanday", "0");
					jsonArray12.put(jsonObject11);
				}
				jsonObject.put("plan", jsonArray11);
				jsonObject.put("follow", jsonArray12);
				responseText = jsonObject.toString();
			}else{
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("startTime", ERROR);
				jsonObject.put("weekTimeId", weekTimeId);
				responseText = jsonObject.toString();
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWorkCompleteCardInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String sdate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -21));
			DataRow row2 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			if(row2 != null && row2.size() > 0){
				currentWeekId = row2.stringValue("WT_ID");
			}
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			JSONArray jsonArray3 = new JSONArray();
			JSONArray jsonArray4 = new JSONArray();
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				jsonArray1.put(row.stringValue("USER_NAME"));
				String userId = row.getString("USER_ID");
				List<DataRow> records = wmWeekManage.findweekCompRecords(sdate,currentDate,userId);
				int weekComp = 0;
				int weekCompAvg = 0;
				for(int j=0;j<records.size();j++){
					DataRow comRow = records.get(j);
					weekComp = weekComp + Integer.valueOf(comRow.stringValue("WW_COMPLETION"));
					int count = records.size();
					if(count == 0){
						count = 1;
					}
					weekCompAvg = weekComp/count;
				}
				jsonArray2.put(weekCompAvg);
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
				if(weekWorkRow != null){
					jsonArray3.put(Integer.valueOf(weekWorkRow.stringValue("WW_COMPLETION")));
				}else{
					jsonArray3.put(0);
				}
			}
			jsonArray4.put(jsonArray2);
			jsonArray4.put(jsonArray3);
			jsonObject.put("labels", jsonArray1);
			jsonObject.put("data", jsonArray4);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer findStatisCurrentWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			DataRow row2 = wmWeekManage.getCurrentWeek(currentDate);
			String currentWeekId = "";
			String startTime = "";
			String endTime = "";
			String standDay = "";
			if(row2 != null && row2.size() > 0){
				currentWeekId = row2.stringValue("WT_ID");
				startTime = row2.stringValue("WT_BEGIN");
				endTime = row2.stringValue("WT_END");
				standDay = row2.stringValue("WT_STAND_DAY");
			}else{
				row2 = wmWeekManage.getCurrentWeekTimeId(currentDate);
				currentWeekId = row2.stringValue("WT_ID");
				startTime = row2.stringValue("WT_BEGIN");
				endTime = row2.stringValue("WT_END");
				standDay = row2.stringValue("WT_STAND_DAY");
			}
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);;
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,currentWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("startTime", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("standDay", standDay);
			jsonObject.put("weekTimeId", currentWeekId);
			jsonObject.put("workComp", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisBeforeWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String weekTimeId = param.get("weekTimeId");
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			String beforeWeekId = "";
			String startTime = "";
			String endTime = "";
			String standDay = "";
			DataRow row1 = wmWeekManage.getBeforeWeekRow(weekTimeId);
			if(row1 != null && row1.size()>0){
				beforeWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				standDay = row1.stringValue("WT_STAND_DAY");
			}else{
				jsonObject.put("week",ERROR);
			}
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,beforeWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,beforeWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("startTime", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("standDay", standDay);
			jsonObject.put("weekTimeId", beforeWeekId);
			jsonObject.put("workComp", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	@PageAction
	public ViewRenderer findStatisFollowWorkCompleteDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String weekTimeId = param.get("weekTimeId");
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			String nextWeekId = "";
			String startTime = "";
			String endTime = "";
			String standDay = "";
			DataRow row1 = wmWeekManage.getNextWeekRow(weekTimeId);
			if(row1 != null && row1.size() > 0){
				nextWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
				standDay = row1.stringValue("WT_STAND_DAY");
			}else{
				jsonObject.put("week",ERROR);
			}
			List<DataRow> userRecords = wmWeekManage.findStaticUserRecords(RoleCode);;
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				String userId = row.getString("USER_ID");
				jsonObject1.put("userName", row.stringValue("USER_NAME"));
				DataRow weekWorkRow = wmWeekManage.getMasterWeekWorkRecord(userId,nextWeekId);
				if(weekWorkRow != null){
					jsonObject1.put("comp",weekWorkRow.stringValue("WW_COMPLETION"));
				}else{
					jsonObject1.put("comp","0");
				}
				DataRow numRow = wmWeekManage.getEntryWorkNumRecord(userId,nextWeekId);
				if(numRow != null){
					jsonObject1.put("count",numRow.stringValue("ENTRY_ID_NUM"));
				}else{
					jsonObject1.put("count","0");
				}
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("startTime", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("standDay", standDay);
			jsonObject.put("workComp", jsonArray1);
			jsonObject.put("weekTimeId", nextWeekId);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserInfos(DataParam param){
		String responseText = FAIL;
		try {
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> userRecords = wmWeekManage.findUserRecords();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<userRecords.size();i++){
				DataRow row = userRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("userId", row.stringValue("USER_ID"));
				jsonObject1.put("userName",row.stringValue("USER_NAME"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("userInfos", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWeekWorkCardInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONArray jsonArray1 = new JSONArray();
			
			String edate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String beginDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -60));
			List<DataRow> records = wmWeekManage.findweekCompRecords(beginDate,edate,userId);
			for(int i=0;i<records.size();i++){
				DataRow row = records.get(i);
				String weekComp = row.stringValue("WW_COMPLETION");
				jsonArray1.put(weekComp);
			}
			if(records.size() < 8){
				int size = 8 - records.size();
				for(int i=0;i<size;i++){
					jsonArray1.put("0");
				}
			}
			
			jsonArray.put(jsonArray1);
			jsonObject.put("data", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findStatisWeekWorkDetailInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			String sdate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -60));
			String edate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			List<DataRow> records = wmWeekManage.findweekCompRecords(sdate,edate,userId);
			for(int i=0;i<records.size();i++){
				JSONObject jsonObject1 = new JSONObject();
				DataRow row = records.get(i);
				String weekComp = row.stringValue("WW_COMPLETION");
				String beginDate = row.stringValue("WT_BEGIN").substring(5, 10).replaceAll("-", "/");
				String endDate = row.stringValue("WT_END").substring(5, 10).replaceAll("-", "/");
				jsonObject1.put("beginDate", beginDate);
				jsonObject1.put("endDate", endDate);
				if(!weekComp.isEmpty()){
					jsonObject1.put("comp", weekComp);
				}else{
					jsonObject1.put("comp", "0");
				}
				
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("weekWork", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initWeekExaminationUserInfos(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> userRecords =  wmWeekManage.findWeekExaminationUserInfos(grpId);
			
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			JSONArray jsonArray2 = new JSONArray();
			if(userRecords.size() > 0){
				for(int i=0;i<userRecords.size();i++){
					DataRow row = userRecords.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("userId", row.stringValue("USER_ID"));
					jsonObject1.put("userName",row.stringValue("USER_NAME"));
					jsonObject1.put("userCode", row.stringValue("USER_CODE"));
					jsonArray1.put(jsonObject1);
					jsonArray2.put(row.stringValue("USER_CODE"));
				}
			}else{
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("userName","无记录");
				jsonArray1.put(jsonObject1);
				jsonArray2.put("无记录");
			}
			
			jsonObject.put("userInfos", jsonArray1);
			jsonObject.put("userCodes", jsonArray2);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initWeekExamGroupInfos(DataParam param){
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> groupRecords = wmWeekManage.initGroupRecords(userId);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<groupRecords.size();i++){
				DataRow row = groupRecords.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("grpId", row.stringValue("GRP_ID"));
				jsonObject1.put("grpName",row.stringValue("GRP_NAME"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("groupInfos", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findActiveUserId(DataParam param){
		String responseText = FAIL;
		try {
			String userCode = param.get("userCode");
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			DataRow userInfo = wmWeekManage.findActiveUserId(userCode);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("userId", userInfo.stringValue("USER_ID"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findCurrentGroupUserCodes(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray1 = new JSONArray();
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			List<DataRow> records =   wmWeekManage.findCurrentGroupUserCodes(grpId);
			if(records.size() >0){
				for(int i=0;i<records.size();i++){
					DataRow row1 = records.get(i);
					jsonArray1.put(row1.stringValue("USER_CODE"));
				}
			}else{
				jsonArray1.put("无记录");
			}
			
			jsonObject.put("curUserCodes", jsonArray1);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer doSubmitWeekWork(DataParam param){
		String responseText = FAIL;
		try {
			String weekWorkId = param.get("weekWorkId");
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			if("null".equals(weekWorkId) || weekWorkId != null){
				wmWeekManage.doSubmitWeekWork(weekWorkId,State);
				
		    	wmWeekManage.doSubmitEntryWeekWork(weekWorkId,ENTRYSTATE);
				responseText = SUCCESS;
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initCurrentSelectWeekInfo(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			String weekWorkId = param.get("weekWorkId");
			String userId = param.get("userId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
			String grpName = grpRow.stringValue("GRP_NAME");
			
			DataRow weekManageRow = wmWeekManage.getCurrentSelectWeekInfo(weekWorkId);
			String weekTimeId = weekManageRow.stringValue("WT_ID");
			DataRow row1 = wmWeekManage.getNextWeekRow(weekTimeId);
			String startTime = row1.stringValue("WT_BEGIN");
			String endTime = row1.stringValue("WT_END");
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("startTime", startTime);
			jsonObject.put("time", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("weekTimeId", weekTimeId);
			
			jsonObject.put("grpName", grpName);
			
			jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
			jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
			jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
			jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
			String completion =  weekManageRow.stringValue("WW_COMPLETION");
			if("0".equals(completion)){
				jsonObject.put("completion","");
			}else{
				jsonObject.put("completion",completion);
			}
			
			List<DataRow> followWeekRecords = wmWeekManage.getWeekWorkRecord(userId,weekTimeId);
			JSONArray jsonArray11 = new JSONArray();
			if(followWeekRecords.size() != 0){
				for(int i=0;i<followWeekRecords.size();i++){
					DataRow row = followWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("describe", "无记录");
				jsonObject11.put("planday", "0");
				jsonArray11.put(jsonObject11);
			}
			
			List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,weekTimeId);
			JSONArray jsonArray12 = new JSONArray();
			if(prepareWeekRecords.size() != 0){
				for(int i=0;i<prepareWeekRecords.size();i++){
					DataRow row = prepareWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
					jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
					jsonArray12.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("predesc", "无记录");
				jsonObject11.put("preplanday", "0");
				jsonArray12.put(jsonObject11);
			}
			jsonObject.put("plan", jsonArray11);
			jsonObject.put("follow", jsonArray12);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer initCurSelectTimeWeekInfo(DataParam param){
		String responseText = FAIL;
		try {
			String grpId = param.get("grpId");
			String userId = param.get("userId");
			String weekTimeId = param.get("weekTimeId");
			User user = (User)this.getUser();
			if(null == userId){
				userId = user.getUserId();
			}
			WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
			String grpName = "";
			if(null != grpId){
				DataRow grpRow = wmWeekManage.queryGroupRecord(grpId);
				grpName = grpRow.stringValue("GRP_NAME");
			}
			
			DataRow row1 = wmWeekManage.getCurrentWeekRow(weekTimeId);
			String currentDate = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String currentWeekId = "";
			String startTime = "";
			String endTime = "";
			if(row1 != null && row1.size() > 0){
				currentWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}else{
				row1 = wmWeekManage.getCurrentWeekTimeId(currentDate);
				currentWeekId = row1.stringValue("WT_ID");
				startTime = row1.stringValue("WT_BEGIN");
				endTime = row1.stringValue("WT_END");
			}
			DataRow weekManageRow = wmWeekManage.getMasterWeekWorkRecord(userId,currentWeekId);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("startTime", startTime);
			jsonObject.put("time", startTime);
			jsonObject.put("endTime", endTime);
			jsonObject.put("weekTimeId", currentWeekId);
			
			jsonObject.put("grpName", grpName);
			if(weekManageRow != null){
				jsonObject.put("wwDay", weekManageRow.stringValue("WW_DAY"));
				jsonObject.put("weekWorkId", weekManageRow.stringValue("WW_ID"));
				jsonObject.put("state", weekManageRow.stringValue("WW_STATE"));
				jsonObject.put("stateName", weekManageRow.stringValue("CODE_NAME"));
				String completion =  weekManageRow.stringValue("WW_COMPLETION");
				if("0".equals(completion)){
					jsonObject.put("completion","");
				}else{
					jsonObject.put("completion",completion);
				}
			}else{
				jsonObject.put("wwDay", "0");
				jsonObject.put("weekWorkId","无记录");
				jsonObject.put("stateName", "空");
			}
			
			List<DataRow> weekWorkRecords = wmWeekManage.getWeekWorkRecord(userId,currentWeekId);
			JSONArray jsonArray11 = new JSONArray();
			if(weekWorkRecords.size() != 0){
				for(int i=0;i<weekWorkRecords.size();i++){
					DataRow row = weekWorkRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("describe", row.stringValue("ENTRY_DESCRIBE"));
					jsonObject11.put("planday", row.stringValue("ENTRY_PLAN"));
					jsonArray11.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("describe", "无记录");
				jsonObject11.put("planday", "0");
				jsonArray11.put(jsonObject11);
			}
			
			List<DataRow> prepareWeekRecords = wmWeekManage.getWeekPrepareRecord(userId,currentWeekId);
			JSONArray jsonArray12 = new JSONArray();
			if(prepareWeekRecords.size() != 0){
				for(int i=0;i<prepareWeekRecords.size();i++){
					DataRow row = prepareWeekRecords.get(i);
					JSONObject jsonObject11 = new JSONObject();
					jsonObject11.put("predesc", row.stringValue("PRE_DESCRIBE"));
					jsonObject11.put("preplanday", row.stringValue("PRE_LOAD"));
					jsonArray12.put(jsonObject11);
				}
			}else{
				JSONObject jsonObject11 = new JSONObject();
				jsonObject11.put("predesc", "无记录");
				jsonObject11.put("preplanday", "0");
				jsonArray12.put(jsonObject11);
			}
			jsonObject.put("plan", jsonArray11);
			jsonObject.put("follow", jsonArray12);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer showSubmitBtn(DataParam param){
		String responseText = FAIL;
		try {
			String selectUserCode = param.get("userCode");
			String grpId = param.get("grpId");
			User user = (User)this.getUser();
			String userCode = user.getUserCode();
			String userId = user.getUserId();
			
			JSONObject jsonObject = new JSONObject();
			if(!userCode.equals(selectUserCode)){
				WmWeekManage wmWeekManage = this.lookupService(WmWeekManage.class);
				List<DataRow> empJobRecords = wmWeekManage.quertUserEmpJobRecords(userId,grpId);
				for(int i=0;i<empJobRecords.size();i++){
					DataRow row = empJobRecords.get(i);
					String empJob = row.stringValue("EMP_JOB");
					if("Master".equals(empJob)){
						jsonObject.put("showSubmitBtn", true);
					}else if("Auditer".equals(empJob)){
						jsonObject.put("showSubmitBtn", true);
					}else{
						jsonObject.put("showSubmitBtn", false);
					}
				}
			}else{
				jsonObject.put("showSubmitBtn",false);
			}
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
}
